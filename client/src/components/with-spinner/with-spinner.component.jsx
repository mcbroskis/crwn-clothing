import React from 'react';

import Spinner from '../spinner/spinner.component';

// Utilizing Higher Order Components here
const WithSpinner = WrapperComponent => ({ isLoading, ...otherProps }) => {
    return isLoading ? <Spinner/> : <WrapperComponent {...otherProps} />
};


export default WithSpinner;